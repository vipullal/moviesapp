package com.fareastsoftware.testapps.moviesapp.models;

import android.media.Image;

import com.google.gson.annotations.SerializedName;

public class Movie {
    @SerializedName("adult")
    Boolean adult;

    @SerializedName("id")
    int id;


    @SerializedName("title")
    String title;

    @SerializedName("overview")
    String overview;

    @SerializedName("poster_path")
    String posterPath;


    @SerializedName("release_date")
    String releaseDate;

    Image mPosterImage = null;



    public Boolean getAdult() {
        return adult;
    }

    public void setAdult(Boolean adult) {
        this.adult = adult;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }
}
