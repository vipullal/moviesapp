package com.fareastsoftware.testapps.moviesapp.ui;

import android.graphics.Bitmap;
import android.os.Bundle;

import com.fareastsoftware.testapps.moviesapp.databinding.ActivityMovieDetailsBinding;
import com.fareastsoftware.testapps.moviesapp.viewmodels.MovieDetailsViewModel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.fareastsoftware.testapps.moviesapp.R;
import android.content.Intent;


public class MovieDetailsActivity extends AppCompatActivity {

    ActivityMovieDetailsBinding mBindings;


    // For now, we only want these fields. If we want more details, we can save the
    // Movie instance in the Application instance or something.
    public static String MOVIE_ID = "id";
    public static String MOVIE_TITLE = "title";
    public static String MOVIE_DESCRIPTION = "description";
    public static String MOVIE_RELEASE_DATE = "release_date";
    public static String MOVIE_POSTER_URL = "poster_url";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setTitle( R.string.movie_details_title);
        ViewModelProvider p = new ViewModelProvider(this.getViewModelStore(), ViewModelProvider.AndroidViewModelFactory.getInstance( getApplication()));
        MovieDetailsViewModel aModel = p.get( MovieDetailsViewModel.class );

        // Sanity checks...
        Intent givenIntent = getIntent();
        assert (givenIntent.hasExtra( MOVIE_TITLE ));
        assert (givenIntent.hasExtra( MOVIE_DESCRIPTION ));
        assert (givenIntent.hasExtra( MOVIE_RELEASE_DATE ));
        assert (givenIntent.hasExtra( MOVIE_POSTER_URL ));


        aModel.setTitle( givenIntent.getStringExtra( MOVIE_TITLE ));
        aModel.setDescription( givenIntent.getStringExtra( MOVIE_DESCRIPTION ));
        aModel.setReleaseDate( givenIntent.getStringExtra( MOVIE_RELEASE_DATE ));
        aModel.setPosterURL( givenIntent.getStringExtra( MOVIE_POSTER_URL ));


        mBindings = DataBindingUtil.setContentView( this, R.layout.activity_movie_details );
        mBindings.setMovieDetailsViewModel( aModel );

        aModel.getDownloadCompleteFlag().observe( this, new Observer<Boolean>(){
            @Override
            public void onChanged(Boolean flag) {
                if( flag ){
                    Bitmap bm = mBindings.getMovieDetailsViewModel().getPosterImage();
                    mBindings.posterView.setImageBitmap( bm );
                }
            }
        });
        aModel.downloadPosterImage();
    }

}
