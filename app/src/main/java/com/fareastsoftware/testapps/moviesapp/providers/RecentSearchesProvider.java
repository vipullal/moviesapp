package com.fareastsoftware.testapps.moviesapp.providers;

import android.content.SearchRecentSuggestionsProvider;

public class RecentSearchesProvider extends SearchRecentSuggestionsProvider {
    public final static String AUTHORITY = "com.fareastsoftware.testapps.moviesapp.providers.RecentSearchesProvider";
    public final static int MODE = DATABASE_MODE_QUERIES;

    public RecentSearchesProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }
}
