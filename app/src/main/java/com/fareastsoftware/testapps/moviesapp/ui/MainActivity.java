package com.fareastsoftware.testapps.moviesapp.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.SearchView;
import android.widget.Toast;

import com.fareastsoftware.testapps.moviesapp.R;
import com.fareastsoftware.testapps.moviesapp.databinding.ActivityMainBinding;
import com.fareastsoftware.testapps.moviesapp.models.Movie;
import com.fareastsoftware.testapps.moviesapp.ui.adapters.MoviesListAdapter;
import com.fareastsoftware.testapps.moviesapp.viewmodels.MoviesListViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity implements MoviesListAdapter.OnItemClickedListener {

    private static String TAG = "MainActivity";

    ActivityMainBinding mBinding;
    AlertDialog mDlgLoading;
    List<Movie> mCurrentList;


    // -------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewModelProvider p = new ViewModelProvider(this.getViewModelStore(), ViewModelProvider.AndroidViewModelFactory.getInstance( getApplication()));
        MoviesListViewModel aModel = p.get( MoviesListViewModel.class );

        mBinding = DataBindingUtil.setContentView( this, R.layout.activity_main );
        mBinding.setViewModel( aModel );



        // display a progress dialog whenever data is loading.
        aModel.getModelIsLoadingFlag().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean isLoading) {
                if( isLoading){
                    showLoadingDialog();
                }
                else{
                    if( MainActivity.this.mDlgLoading != null ){
                        MainActivity.this.mDlgLoading.dismiss();
                        MainActivity.this.mDlgLoading = null;
                    }
                }
            }
        });


        // Observe changes to the list.
        aModel.getMoviesLiveData().observe( this, new Observer<List<Movie>>(){

            @Override
            public void onChanged(List<Movie> movies) {
                refreshMoviesList( movies);
            }
        });


        MoviesListAdapter adapter = new MoviesListAdapter( this );

        mBinding.moviesList.setAdapter( adapter );
        mBinding.moviesList.setLayoutManager(new LinearLayoutManager(this));
        mBinding.moviesList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if( newState == RecyclerView.SCROLL_STATE_IDLE){
                    if( !recyclerView.canScrollVertically( -1 )){
                        scrollToPreviousPage();
                    }
                    else if( !recyclerView.canScrollVertically( 0 ) ){
                        scrollToNextPage();
                    }
                }

            }
        });

        aModel.loadMovies();

    }


    // ----------------------------------------------------
    void scrollToNextPage(){
        if(!this.mBinding.getViewModel().getNextPage() ){
            Toast.makeText( this, R.string.at_bottom_of_list, Toast.LENGTH_SHORT).show();
        };
    }


    // ----------------------------------------------------
    void scrollToPreviousPage(){
        if(!this.mBinding.getViewModel().getPreviousPage()){
            Toast.makeText( this, R.string.at_top_of_list, Toast.LENGTH_SHORT).show();
        };
    }



    // -------------------------------------
    void refreshMoviesList( List<Movie> moviesList){
        int curPage = this.mBinding.getViewModel().getCurrentPage();
        int numPages = this.mBinding.getViewModel().getTotalPages();
        if( numPages != 0 ){
            mBinding.txtPageCounter.setText( String.format("page: %d of %d", curPage, numPages));
        }
        else{
            mBinding.txtPageCounter.setText( String.format("page: %d", curPage));
        }
        MoviesListAdapter adapter = (MoviesListAdapter)mBinding.moviesList.getAdapter();
        adapter.setData( moviesList );
        mBinding.moviesList.scrollToPosition( 0 );
    }


    // -------------------------------------
    void showLoadingDialog(){
        AlertDialog.Builder b = new AlertDialog.Builder( this )
            .setMessage( R.string.ma_loading_msg)
            .setTitle( R.string.ma_loading_title);
        this.mDlgLoading = b.create();
        this.mDlgLoading.show();
    }


    // -------------------------------------
    // Menu:
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.requestFocus();
        ComponentName cn = new ComponentName( this, SearchResultsActivity.class );
        SearchableInfo inf = searchManager.getSearchableInfo(cn);
        searchView.setSearchableInfo( inf );
        return true;
    }


    // -------------------------------------
    @Override
    public void onItemClicked( Movie m) {
        Intent movieDetailsIntent = new Intent( this, MovieDetailsActivity.class );
        movieDetailsIntent.putExtra(MovieDetailsActivity.MOVIE_ID, m.getId() );
        movieDetailsIntent.putExtra(MovieDetailsActivity.MOVIE_TITLE, m.getTitle() );
        movieDetailsIntent.putExtra(MovieDetailsActivity.MOVIE_DESCRIPTION, m.getOverview());
        movieDetailsIntent.putExtra(MovieDetailsActivity.MOVIE_RELEASE_DATE, m.getReleaseDate());
        movieDetailsIntent.putExtra(MovieDetailsActivity.MOVIE_POSTER_URL, m.getPosterPath());
        startActivity( movieDetailsIntent );
    }
}
