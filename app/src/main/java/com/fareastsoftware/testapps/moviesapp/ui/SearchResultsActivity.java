package com.fareastsoftware.testapps.moviesapp.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fareastsoftware.testapps.moviesapp.R;
import com.fareastsoftware.testapps.moviesapp.databinding.ActivitySearchResultsBinding;
import com.fareastsoftware.testapps.moviesapp.models.Movie;
import com.fareastsoftware.testapps.moviesapp.providers.RecentSearchesProvider;
import com.fareastsoftware.testapps.moviesapp.ui.adapters.SearchResultsListAdapter;
import com.fareastsoftware.testapps.moviesapp.viewmodels.SearchResultsViewModel;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.util.Log;
import android.widget.Toast;

import java.net.URLEncoder;
import java.util.List;

public class SearchResultsActivity extends AppCompatActivity {


    private static String TAG = "SearchResultsActivity";
    ActivitySearchResultsBinding mBinding;
    AlertDialog mDlgLoading;
    String mMovieTitle = "Rambo";
    String mMovieTitleUrlEncoded;



    //-----------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle( R.string.search_results_title );

        // get the search string from the intent
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            // todo: Validations!
            String query = intent.getStringExtra(SearchManager.QUERY);
            this.mMovieTitle = query;
            String urlEncQuery ="";
            try{
                urlEncQuery = URLEncoder.encode(query, "utf-8"  );
            }
            catch( java.io.UnsupportedEncodingException ex){
                Log.e(TAG, "Oops, we got an UnsupportedEncodingException! ");
            }
            mMovieTitleUrlEncoded = urlEncQuery;
        }
        else{
            mMovieTitleUrlEncoded = "Rambo";
            mMovieTitle = mMovieTitleUrlEncoded;
        }

        ViewModelProvider p = new ViewModelProvider( this.getViewModelStore(), ViewModelProvider.AndroidViewModelFactory.getInstance( getApplication()));
        SearchResultsViewModel vm = p.get( SearchResultsViewModel.class );
        vm.getModelisLoading().observe( this, new Observer<Boolean>(){
            @Override
            public void onChanged(Boolean isLoading) {
                if( isLoading ){
                    showLoadingAlert();
                }
                else{
                    refreshView();
                }
            }
        });

        mBinding = DataBindingUtil.setContentView( this, R.layout.activity_search_results );
        mBinding.setViewModel( vm );

        mBinding.rvSearchResults.setLayoutManager(new LinearLayoutManager(this));
        mBinding.rvSearchResults.setAdapter( new SearchResultsListAdapter( null ));
        vm.loadSearchResults( mMovieTitleUrlEncoded );

        mBinding.rvSearchResults.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if( newState == RecyclerView.SCROLL_STATE_IDLE){
                    if( !recyclerView.canScrollVertically( -1 )){
                        scrollToPreviousPage();
                    }
                    else if( !recyclerView.canScrollVertically( 0 ) ){
                        scrollToNextPage();
                    }
                }

            }
        });
    }


    // ----------------------------------------------------
    void scrollToNextPage(){
        if(!this.mBinding.getViewModel().getNextPage() ){
            Toast.makeText( this, R.string.at_bottom_of_list, Toast.LENGTH_SHORT).show();
        };
    }


    // ----------------------------------------------------
    void scrollToPreviousPage(){
        if(!this.mBinding.getViewModel().getPreviousPage()){
            Toast.makeText( this, R.string.at_top_of_list, Toast.LENGTH_SHORT).show();
        };
    }



    //-----------------------------------------------------
    void showLoadingAlert(){
        AlertDialog.Builder b = new AlertDialog.Builder( this )
                .setMessage( R.string.ma_loading_msg)
                .setTitle( R.string.ma_loading_title);
        this.mDlgLoading = b.create();
        this.mDlgLoading.show();
    }





    //-----------------------------------------------------
    void refreshView(){
        if( mDlgLoading != null ){
            mDlgLoading.dismiss();
            mDlgLoading = null;
        }

        int curPage = this.mBinding.getViewModel().getCurrentPage();
        int numPages = this.mBinding.getViewModel().getMaxPages();
        if( numPages != 0 ){
            mBinding.txtPageCounter.setText( String.format("page: %d of %d", curPage, numPages));
        }
        else{
            mBinding.txtPageCounter.setText( String.format("page: %d", curPage));
        }
        List<Movie> foundMovies = this.mBinding.getViewModel().getMovies();

        // successful search, save the string.
        if( foundMovies.size() != 0 ){
            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this, RecentSearchesProvider.AUTHORITY, RecentSearchesProvider.MODE);
            suggestions.saveRecentQuery( this.mMovieTitle , null);
            SearchResultsListAdapter adapter = (SearchResultsListAdapter)mBinding.rvSearchResults.getAdapter();
            adapter.setData( foundMovies);
            mBinding.rvSearchResults.invalidate();
            mBinding.rvSearchResults.scrollToPosition(0);
        }
        else{
            AlertDialog.Builder b = new AlertDialog.Builder(this)
                    .setTitle(R.string.no_search_results_title)
                    .setMessage( R.string.no_search_results_msg)
                    .setCancelable( false )
                    .setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    SearchResultsActivity.this.finish();
                                }
                            }
                    );
            AlertDialog d = b.create();
            d.show();
        }

    }

}
