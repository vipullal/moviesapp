package com.fareastsoftware.testapps.moviesapp.viewmodels;

import android.util.Log;

import com.fareastsoftware.testapps.moviesapp.models.Movie;
import com.fareastsoftware.testapps.moviesapp.models.MoviesList;
import com.fareastsoftware.testapps.moviesapp.repository.MoviesRepository;

import java.util.List;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoviesListViewModel extends ViewModel  {

    static final String TAG = "MoviesListViewModel";

    MutableLiveData<Boolean> mModelisLoading = new MutableLiveData<>();

    int mCurrentPage = 1;
    int mTotalPages;
    MutableLiveData<List<Movie>> mMoviesData = new MutableLiveData<>();



    // ---------------------------------------------
    public MoviesListViewModel(){
    }


    // ---------------------------------------------
    public MutableLiveData<Boolean> getModelIsLoadingFlag() {
        return mModelisLoading;
    }


    // ---------------------------------------------
    public MutableLiveData< List<Movie>> getMoviesLiveData() {
        return mMoviesData;
    }



    // --------------------------------------------
    public boolean getNextPage(){
        if( this.mCurrentPage < this.mTotalPages ){
            ++this.mCurrentPage;
            this.loadMovies();
            return true;
        }
        else{
            return false;
        }
    }


    // --------------------------------------------
    public boolean getPreviousPage(){
        if( this.mCurrentPage >=2 ){
            --this.mCurrentPage;
            this.loadMovies();
            return true;
        }
        else{
            return false;
        }
    }


    // ---------------------------------------------
    public int getCurrentPage() {
        return mCurrentPage;
    }

    // ---------------------------------------------
    public void setCurrentPage(int mCurrentPage) {
        this.mCurrentPage = mCurrentPage;
    }

    // ---------------------------------------------
    public int getTotalPages() {
        return mTotalPages;
    }

    // ---------------------------------------------
    public void setTotalPages(int mTotalPages) {
        this.mTotalPages = mTotalPages;
    }

    // ---------------------------------------------
    public List<Movie> getMoviesList() {
        return mMoviesData.getValue();
    }

    // ---------------------------------------------
    public void setMoviesList(List<Movie> mMoviesList) {
        this.mMoviesData.setValue( mMoviesList );
    }

    // ---------------------------------------------
    private  void onNewPageLoaded( MoviesList response ){
        this.mCurrentPage = response.getCurrentPage();
        this.mTotalPages = response.getMaxPages();
        this.setMoviesList( response.getMovies() );
    }


    // ---------------------------------------------
    public void loadMovies(){
        this.mModelisLoading.setValue( true );
        MoviesRepository.getInstance().getMovies( mCurrentPage ).enqueue(new Callback<MoviesList>() {
            @Override
            public void onResponse(Call<MoviesList> call, Response<MoviesList> response) {
                MoviesListViewModel.this.mModelisLoading.setValue( false );
                if( response.body() != null && (response.body() instanceof  MoviesList)  ){
                    Log.d(TAG, "Success");
                    onNewPageLoaded( (MoviesList)response.body() );
                }
                else{

                    // todo: handle error
                }
            }

            @Override
            public void onFailure(Call<MoviesList> call, Throwable t) {
                MoviesListViewModel.this.mModelisLoading.setValue( false );
                // todo: handle failure
            }
        });
    }
}
