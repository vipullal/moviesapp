package com.fareastsoftware.testapps.moviesapp.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.fareastsoftware.testapps.moviesapp.R;
import com.fareastsoftware.testapps.moviesapp.models.Movie;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MoviesListAdapter extends RecyclerView.Adapter<MoviesListAdapter.MyViewHolder> {


    List<Movie> mMoviesList = new ArrayList<Movie>();


    public interface OnItemClickedListener {
        void onItemClicked( Movie m );
    }

    OnItemClickedListener mListener = null;

    // -----------------------------------------------------------
    public MoviesListAdapter( OnItemClickedListener inListener ){
        mListener = inListener;
    }



    // -----------------------------------------------------------
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.from( parent.getContext()).inflate(R.layout.activity_main_line_item, parent, false );
        return new MyViewHolder( itemView );
    }


    // -----------------------------------------------------------
    public void setData( List<Movie> inList ){
        this.mMoviesList = inList;
        this.notifyDataSetChanged();
    }

    // -----------------------------------------------------------
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Movie mov = this.mMoviesList.get( position );
        holder.bind( mov, mListener );
    }

    // -----------------------------------------------------------
    @Override
    public int getItemCount() {
        return mMoviesList.size();
    }



    // ==============================================
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView mTxtMovieName;
        private TextView mTxtMovieDescription;
        private Movie movie;


        public MyViewHolder(View v) {
            super(v);
            this.mTxtMovieName = v.findViewById( R.id.txtMovieName);
            this.mTxtMovieDescription = v.findViewById( R.id.txtMovieDescription);
        }

        void bind( Movie mov, final OnItemClickedListener listener ){
            this.mTxtMovieName.setText( mov.getTitle());
            this.mTxtMovieDescription.setText( (mov.getOverview() ));
            this.movie = mov;
            this.itemView.setOnClickListener( new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    if( listener != null ){
                        listener.onItemClicked( movie );
                    }
                }
            });
        }
    }

}
