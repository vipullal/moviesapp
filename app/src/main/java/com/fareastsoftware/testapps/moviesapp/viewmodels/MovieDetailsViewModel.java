package com.fareastsoftware.testapps.moviesapp.viewmodels;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;

import com.fareastsoftware.testapps.moviesapp.models.Movie;
import com.fareastsoftware.testapps.moviesapp.repository.MoviesRepository;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieDetailsViewModel extends ViewModel {

    String title;
    String description;
    String releaseDate;
    String posterURL;
    Bitmap posterImage;

    MutableLiveData<Boolean> mImageLoaded = new MutableLiveData<>();

    public String getDescription() {
        return description;
    }


    public String getReleaseDate() {
        return releaseDate;
    }


    public String getTitle() {
        return title;
    }


    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public void setPosterURL(String posterURL) {
        this.posterURL = posterURL;
    }

    public MutableLiveData<Boolean> getDownloadCompleteFlag(){
        return this.mImageLoaded;
    }

    // -------------------------------------------
    public Bitmap getPosterImage(){
        return this.posterImage;
    }

    // -------------------------------------------
    public void downloadPosterImage(){
        mImageLoaded.setValue( false );
        MoviesRepository.getInstance().downloadImage( posterURL, 500).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if( response.body() != null){
                    posterImage = BitmapFactory.decodeStream(response.body().byteStream());
                    mImageLoaded.setValue( true );
                }
                else{
                    mImageLoaded.setValue( false );
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mImageLoaded.setValue( false ); // todo: setup a meaningful error message which the UI can display
            }
        });
    }
}
