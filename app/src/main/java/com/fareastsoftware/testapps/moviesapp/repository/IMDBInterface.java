package com.fareastsoftware.testapps.moviesapp.repository;

import com.fareastsoftware.testapps.moviesapp.models.MoviesList;
import com.fareastsoftware.testapps.moviesapp.models.SearchResults;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface IMDBInterface {

    @GET
    public Call<MoviesList> getMovies(@Url String url );

    @GET
    public Call<ResponseBody> getPosterImage(@Url String url);

    @GET
    public Call<SearchResults> searchMovies(@Url String url);

}
