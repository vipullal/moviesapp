package com.fareastsoftware.testapps.moviesapp.repository;

import com.fareastsoftware.testapps.moviesapp.models.Movie;
import com.fareastsoftware.testapps.moviesapp.models.MoviesList;
import com.fareastsoftware.testapps.moviesapp.models.SearchResults;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MoviesRepository {

    IMDBInterface mMoviesEndPoint;
    Retrofit mRetrofit;

    /*
    String apiKey = "261c6e155426f3f1c4b2270f8f2ba079";
    String baseURL = "https://api.themoviedb.org/3/";
    String imagesBaseURL ="https://image.tmdb.org/t/p/w92/";
    String searchMoviesBaseURL = "https://api.themoviedb.org/3/search/movie";
    */

    String apiKey = null;
    String baseURL = null;
    String imagesBaseURL = null;
    String searchMoviesBaseURL = null;

    static MoviesRepository sInstance;

    public static MoviesRepository getInstance(){
        return sInstance;
    }




    public static class Initializer {
        String apiKey = null;
        String baseURL = null ;
        String imagesBaseURL = null ;
        String searchesBaseURL = null;


        public Initializer(){}

        public Initializer doInit(){
            assert(apiKey != null );
            assert(baseURL != null) ;
            assert(imagesBaseURL != null );
            assert(searchesBaseURL != null);
            sInstance = new MoviesRepository();
            sInstance.apiKey = this.apiKey;
            sInstance.baseURL = this.baseURL;
            sInstance.imagesBaseURL = this.imagesBaseURL;
            sInstance.searchMoviesBaseURL = this.searchesBaseURL;
            sInstance.initSelf();
            return this;
        }


        public Initializer setApiKey(String apiKey) {
            this.apiKey = apiKey;
            return this;
        }

        public Initializer setBaseURL(String baseURL) {
            this.baseURL = baseURL;
            return  this;
        }

        public Initializer setImagesBaseURL(String imagesBaseURL) {
            this.imagesBaseURL = imagesBaseURL;
            return this;
        }

        public Initializer setSearchesBaseURL(String searchesBaseURL) {
            this.searchesBaseURL = searchesBaseURL;
            return this;
        }
    }


    MoviesRepository() {
    }

    // -------------------------------
    void initSelf(){

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor( logging )
            .build();

        mRetrofit = new Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory( GsonConverterFactory.create() )
                .client( client )
                .build();
        mMoviesEndPoint = mRetrofit.create( IMDBInterface.class );
    }






    // -------------------------------
    // curl https://api.themoviedb.org/3/discover/movie?api_key=261c6e155426f3f1c4b2270f8f2ba079
    public Call<MoviesList> getMovies( int page ){
        String urlStr = String.format("%sdiscover/movie?api_key=%s&page=%d", baseURL, apiKey, page );
        return mMoviesEndPoint.getMovies( urlStr );
    }


    // -------------------------------
    // curl https://api.themoviedb.org/3/discover/movie?api_key=261c6e155426f3f1c4b2270f8f2ba079
    public Call<Movie> getMovieWithId(){
        String urlStr = String.format("%sdiscover/movie?api_key=%s", baseURL, apiKey );
        return null;
    }


    // -------------------------------
    // curl 'https://api.themoviedb.org/3/search/movie?api_key=261c6e155426f3f1c4b2270f8f2ba079&query=jackson&page=1&include_adult=true'
    public Call<SearchResults> searchMovies( String title, int page ){
        String urlStr = String.format("%s?api_key=%s&query=%s&page=%d", searchMoviesBaseURL, apiKey, title, page );
        return mMoviesEndPoint.searchMovies( urlStr );
    }


    // -------------------------------
    // curl -o some_image_w500.jpg https://image.tmdb.org/t/p/w500/7D430eqZj8y3oVkLFfsWXGRcpEG.jpg
    public Call<ResponseBody> downloadImage(String fileName, int size ){

        assert( size == 92 || size ==185 || size ==500 || size == 780);

        String urlStr = String.format("%s/w%d/%s", imagesBaseURL, size, fileName );
        return mMoviesEndPoint.getPosterImage( urlStr );
    }


}
