package com.fareastsoftware.testapps.moviesapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MoviesList {

    @SerializedName("page")
    int mCurrentPage;

    @SerializedName("total_results")
    int mMoviesCount;

    @SerializedName("total_pages")
    int mMaxPages;

    @SerializedName("results")
    List<Movie> mMovies;


    public int getCurrentPage() {
        return mCurrentPage;
    }

    public void setCurrentPage(int mCurrentPage) {
        this.mCurrentPage = mCurrentPage;
    }

    public int getMoviesCount() {
        return mMoviesCount;        // TODO: Do we really need this?
    }

    public void setMoviesCount(int numMovies) {
        this.mMoviesCount = numMovies;
    }

    public int getMaxPages() {
        return mMaxPages;
    }

    public void setMaxPages(int pages) {
        this.mMaxPages = pages;
    }

    public List<Movie> getMovies() {
        return mMovies;
    }

    public void setMovies(List<Movie> mMovies) {
        this.mMovies = mMovies;
    }

    // to keep the seralizer happy!
    public void setTotal_results(int total_results){
        this.mMoviesCount = total_results;
    }
}
