package com.fareastsoftware.testapps.moviesapp.viewmodels;

import com.fareastsoftware.testapps.moviesapp.models.Movie;
import com.fareastsoftware.testapps.moviesapp.models.SearchResults;
import com.fareastsoftware.testapps.moviesapp.repository.MoviesRepository;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchResultsViewModel extends ViewModel {

    MutableLiveData<Boolean> mModelisLoading = new MutableLiveData<>();
    MutableLiveData<Boolean> mPosterImageLoaded = new MutableLiveData<>();


    int currentPage;
    int maxPages;
    int totalMatches;
    int currentItemIndex;
    List<Movie> movies = new ArrayList<Movie>();
    String movieTitle;


    // --------------------------------------------
    public SearchResultsViewModel(){
    }

    // --------------------------------------------
    public MutableLiveData<Boolean> getModelisLoading(){
        return this.mModelisLoading;
    }



    // --------------------------------------------
    private void setupModelData(SearchResults results){
        this.currentPage = results.getPage();
        this.maxPages = results.getTotal_pages();
        this.movies = results.getMovies();
        this.mModelisLoading.setValue( false );
        this.currentItemIndex = 1;
    }

    // -------------------------------------------
    public int getCurrentPage() {
        return currentPage;
    }


    // -------------------------------------------
    public int getMaxPages() {
        return maxPages;
    }


    // --------------------------------------------
    public boolean getNextPage(){
        if( this.currentPage < this.maxPages ){
            ++this.currentPage;
            this.loadSearchResults();
            return true;
        }
        else{
            return false;
        }
    }


    // --------------------------------------------
    public boolean getPreviousPage(){
        if( this.currentPage >=2 ){
            --this.currentPage;
            this.loadSearchResults();
            return true;
        }
        else{
            return false;
        }
    }


    // --------------------------------------------
    public List<Movie> getMovies(){
        return this.movies;
    }


    // --------------------------------------------
    public void downloadImages(){
        // Note: We should be using RxJava here, but then just to add a whole library for
        // just this seems to be an overkill. Resorting to thread.

    }


    // -------------------------------------------
    public void loadSearchResults() {
        this.mModelisLoading.setValue( true );
        MoviesRepository.getInstance().searchMovies( this.movieTitle, this.currentPage ).enqueue(new Callback<SearchResults>() {
            @Override
            public void onResponse(Call<SearchResults> call, Response<SearchResults> response) {
                if( response.body() != null && (response.body() instanceof SearchResults)){
                    setupModelData( response.body() );
                }
                else{
                    mModelisLoading.setValue( false );
                    // todo: handle this...
                }
            }

            @Override
            public void onFailure(Call<SearchResults> call, Throwable t) {
                mModelisLoading.setValue( false );
                // todo: handle failure
            }
        });

    }

    // --------------------------------------------
    public void loadSearchResults(String movieTitle ){
        this.movieTitle = movieTitle;
        this.currentPage = 1;
        loadSearchResults();;
    }

}
