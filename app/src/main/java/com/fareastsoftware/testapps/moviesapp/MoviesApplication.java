package com.fareastsoftware.testapps.moviesapp;

import android.app.Application;
import android.content.res.Resources;
import android.util.Log;

import com.fareastsoftware.testapps.moviesapp.repository.MoviesRepository;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class MoviesApplication extends Application {

    private static String TAG = "MoviesApplication";

    @Override
    public void onCreate() {
        super.onCreate();
        Resources resources = this.getResources();
        Properties properties = new Properties();
        try {
            InputStream rawResource = resources.openRawResource(R.raw.config);
            properties.load(rawResource);
            rawResource.close();
            initMovieRepository( properties );
        }
        catch (Resources.NotFoundException e) {
            Log.e(TAG, "Unable to find the config file: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "Failed to open config file.");
        }

    }


    // -------------------------------------------
    private void initMovieRepository( Properties fromProperties ){
        // Initialize the repository
        MoviesRepository.Initializer b = new MoviesRepository.Initializer()
            .setApiKey( fromProperties.getProperty("apiKey",null ) )
            .setBaseURL( fromProperties.getProperty("baseURL",null ) )
            .setImagesBaseURL( fromProperties.getProperty("imagesBaseURL",null ))
            .setSearchesBaseURL( fromProperties.getProperty("searchMoviesBaseURL",null ) )
            .doInit();
    }
}
